interface Executable {

	void execute();

}

class Runner {

	public void run(Executable e) {

		System.out.println("Executing code block...");
		e.execute();

	}

}

public class Main {

	public static void main(String[] args) {

		Runner runner = new Runner();
		// bez lambda expressions
		// uzywamy anonimowej klasy
		runner.run(new Executable() {
			public void execute() {
				System.out.println("Hello there");
			}
		});
		// powyzej bez lambda expression
		System.out.println("*******************************************");
		// ponizej z lambda expression
		// nadal trzeba uzyc interface ale upraszczamy to co jest zrobione
		// przy pomocy anonimowej funkcji
		runner.run(() -> System.out.println("Hello there"));
	}

}
